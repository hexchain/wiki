# (QEMU) Using a directory as rootfs

Assuming kernel has built-in (non-module) support for virtio, 9p and/or nfs, which the defconfig does.

## With 9p

QEMU argument:
```
-virtfs local,id=root,mount_tag=/dev/root,security_model=passthrough,path=/path/to/rootfs
```

Append the following content to kernel command line:
```
rw rootfstype=9p rootflags=trans=virtio
```

!!! note

    `mount_tag` must be `/dev/root`. There is no need to specify `root=` for kernel.


## With NFS

Ensure the host is reachable through a network device in QEMU, e.g.:
```
-device virtio-net-device,netdev=usernet -netdev user,id=usernet
```

Append the following content to kernel command line:
```
root=/dev/nfs rw rootfstype=nfs nfsroot=<server-ip>:/path/to/nfs/export,vers=3 ip=dhcp
```

!!! note

    `root=` must be `/dev/nfs`.

    The **vers** parameter changes the NFS protocol version from 2 (the
    default) to higher because `nfs-utils` already disabled NFS v2 support in
    2014. See [this Gentoo forum
    post](https://forums.gentoo.org/viewtopic-p-7500302.html?sid=094bde435cbf7616b4d8ecb83ef57caf#7500302).
