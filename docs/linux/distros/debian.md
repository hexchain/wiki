## APT

### Disable PDiffs

`/etc/apt/apt.conf.d/90nopdiffs`:

```
Acquire::PDiffs "no";
```

Note that APT will still download PDiffs if `apt-file` is installed.

### Limit release

`/etc/apt/preferences.d/limit-release`:

```
Package: *
Pin: release n=stretch  # or: a=stable
Pin-Priority: 50
```
