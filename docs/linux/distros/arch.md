## Packaging

### ENOSPC when building with devtools

`devtools` uses `systemd-nspawn`, which by default mounts a tmpfs with `size=10%` to the `/tmp` directory inside the container. This may not be enough for e.g. large LTO-enabled packages.

A possible workaround is to instruct `systemd-nspawn` to not mount a tmpfs and use the directory as is:

```
# SYSTEMD_NSPAWN_TMPFS_TMP=0 extra-x86_64-build
```