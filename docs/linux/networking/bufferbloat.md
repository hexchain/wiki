# Bufferbloat

Taken from https://www.bufferbloat.net/projects/bloat/wiki/Linux_Tips/.

```ini
net.ipv4.tcp_ecn = 1
net.ipv4.tcp_sack = 1
net.ipv4.tcp_dsack = 1

net.ipv4.tcp_congestion_control = bbr
net.core.default_qdisc = fq_codel  # or fq
```

For the difference between `fq_codel` and `fq`, see [here](https://www.bufferbloat.net/projects/codel/wiki/#binary-code-and-kernels-for-linux-based-operating-systems).