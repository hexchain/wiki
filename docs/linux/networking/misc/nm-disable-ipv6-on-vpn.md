# NetworkManager: Disable IPv6 on VPN connection

`/etc/NetworkManager/dispatcher.d/10-disable-ipv6-on-vpn`:

```bash
#!/bin/bash
STATUS="$2"
TABLE_ID=222

if [[ -v VPN_IP6_NUM_ROUTES ]] && [[ $VPN_IP6_NUM_ROUTES -gt 0 ]]; then
    # do nothing if vpn has ipv6 routes
    exit 0
fi

case "$STATUS" in
    vpn-up)
        { ip -6 -force -b - <<EOF
route add prohibit default table $TABLE_ID
route add throw fe80::/64 table $TABLE_ID
rule add from all lookup $TABLE_ID
EOF
        } || true
        echo "IPv6 disabled"
        ;;
    vpn-down)
        { ip -6 -force -b - <<EOF
rule delete from all lookup $TABLE_ID
route flush table $TABLE_ID
EOF
        } || true
        ;;
esac
```