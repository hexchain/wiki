# Stateless NAT with NFTables

```text
define internal = 192.0.2.234
define external = 203.0.113.21

table ip raw {
  chain prerouting {
    type filter hook prerouting priority raw; policy accept;
    # do not conntrack
    ip saddr $internal notrack return
    ip daddr $external notrack return
  }
}

table ip route {
  chain output {
    type filter hook postrouting priority srcnat + 1; policy accept;
    ip saddr $internal ip saddr set $external
  }

  chain prerouting {
    type filter hook prerouting priority dstnat + 1; policy accept;
    ip daddr $external ip daddr set $internal
  }
}
```
