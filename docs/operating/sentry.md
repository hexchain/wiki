# Sentry

!!! warning
    The author of this document is no longer maintaining an active Sentry
    deployment. Information on this page may be severly outdated.

    Use with care!

## General Tips

* Use [pgBouncer](https://pgbouncer.github.io/) and/or enable [persistent database connections](https://docs.djangoproject.com/en/1.11/ref/databases/#persistent-connections).
* Set up all caches, including Django `CACHES` (may reduce 50% database transactions).

## Monitor Celery queues

```python
#!/usr/bin/env python

from __future__ import absolute_import, print_function, unicode_literals

import os
import socket
import time

from sentry.runner import configure


def watch(queues, interval=10):
    from sentry.monitoring.queues import backend

    s = socket.create_connection(("carbon.your.org", 2003))
    while True:
        current = time.time()
        next_run = current + interval
        sizes = backend.bulk_get_sizes(queues)
        metric = "\n".join(["sentry.custom.queues.{}.length {} {}".format(
            q[0].replace(".", "#"), q[1], int(current)) for q in sizes])
        s.sendall(metric)
        time.sleep(next_run - time.time())


def main():
    from django.conf import settings
    queues = [q.name for q in settings.CELERY_QUEUES]
    while True:
        try:
            watch(queues)
        except KeyboardInterrupt:
            break
        except:
            pass


if __name__ == '__main__':
    os.environ.setdefault('SENTRY_CONF', "/etc/sentry")
    configure()
    main()
```
